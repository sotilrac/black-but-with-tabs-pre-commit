Pre-commit hook to be used with pre-commit

Add this to your config file
```yaml
repos:
  - repo: https://gitlab.com/sotilrac/black-but-with-tabs-pre-commit
    rev: v0.1
    hooks:
      - id: black
        language_version: python3 # Should be a command that runs python3.6+
```

This is based on the pip package `black-but-with-tabs-instead-of-spaces` and [black](https://github.com/psf/black)
