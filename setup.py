"""Setup file"""
from setuptools import setup

setup(
	name="pre_commit_dummy_package",
	version="0.0.0",
	install_requires=["black-but-with-tabs-instead-of-spaces"],
)
